package model;

import controller.MenuItem;

import java.io.Serializable;

public class BaseProduct extends java.awt.MenuItem implements MenuItem, Serializable {

    private String title;
    private float rating;
    private int calories;
    private int protein;
    private int fat;
    private int sodium;
    private int price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getProtein() {
        return protein;
    }

    public void setProtein(int protein) {
        this.protein = protein;
    }

    public int getFat() {
        return fat;
    }

    public void setFat(int fat) {
        this.fat = fat;
    }

    public int getSodium() {
        return sodium;
    }

    public void setSodium(int sodium) {
        this.sodium = sodium;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public BaseProduct(String nume, float r, int c, int p, int f, int s, int pr)
    {
        this.title=nume;
        this.rating=r;
        this.calories=c;
        this.protein=p;
        this.fat=f;
        this.sodium=s;
        this.price=pr;
    }

    @Override
    public int computePrice() {
        return this.price;
    }

    @Override
    public String print() {
        String prod=this.title+" "+this.rating+" "+this.calories+" "+this.protein+" "+this.fat+" "+this.sodium+" "+this.price;
        return prod;
    }

    @Override
    public int computeSodium() {
        return this.getSodium();
    }

    @Override
    public int computeFat() {
        return this.getFat();
    }

    @Override
    public int computeProtein() {
        return this.getProtein();
    }

    @Override
    public int computeCalories() {
        return this.getCalories();
    }

    @Override
    public float computeRating() {
        return this.getRating();
    }

    @Override
    public String computeName() {
        return this.title;
    }

    @Override
    public String toString() {
        String prod=this.title+" "+this.price;
        return prod;
    }

    public BaseProduct (String nume)
    {
        this.title=nume;
    }
}
