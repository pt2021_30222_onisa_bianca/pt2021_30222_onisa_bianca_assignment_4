package model;

import controller.MenuItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {
    private int ID;
    private Date data;
    private User client;
    private int price;
    private List<MenuItem> products=new ArrayList<>();

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<MenuItem> getProducts() {
        return products;
    }

    public void setProducts(List<MenuItem> products) {
        this.products = products;
    }

    public String enumProd()
    {
        String rez="";
        for(MenuItem item: this.getProducts())
        {
            rez=rez+item.computeName()+"; ";
        }
        return rez;
    }

    @Override
    public int hashCode()
    {
        int res=17;
        res=res*31+ID;
        res=res*31+data.hashCode();
        return res;
    }

    public void printOrders(List<Order> orders)
    {
        for(Order ord: orders)
        {
            System.out.print("ID: "+String.valueOf(ord.getID())+" Client: "+ord.getClient().getUsername()+" Price: "+String.valueOf(ord.getPrice())+" Date: "+String.valueOf(ord.getData())+" Products:");
            for(MenuItem prod:ord.getProducts())
            {
                System.out.print(", "+prod.computeName());
            }
            System.out.println();
        }
    }
}
