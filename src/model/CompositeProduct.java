package model;

import controller.MenuItem;

import java.io.Serializable;
import java.util.List;

public class CompositeProduct implements controller.MenuItem, Serializable {

    List<controller.MenuItem> menuItemList;
    private int nr=0;
    private String name;
    private int price;
    private float rating;
    private int calories;
    private int protein;
    private int fat;
    private int sodium;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int computePrice() {
        int pr=0;
        for(controller.MenuItem m: menuItemList)
        {
            pr=pr+m.computePrice();
        }
        this.price=pr;
        return pr;
    }

    @Override
    public String print() {
        String prod=this.name+" "+this.price;
        return prod;
    }

    @Override
    public int computeSodium() {
        int sod=0;
        for(controller.MenuItem m: menuItemList)
        {
            sod=sod+m.computeSodium();
        }
        this.sodium=sod;
        return sod;
    }

    @Override
    public int computeFat() {
        int sod=0;
        for(controller.MenuItem m: menuItemList)
        {
            sod=sod+m.computeFat();
        }
        this.fat=sod;
        return sod;
    }

    @Override
    public int computeProtein() {
        int sod=0;
        for(controller.MenuItem m: menuItemList)
        {
            sod=sod+m.computeProtein();
        }
        this.protein=sod;
        return sod;
    }

    @Override
    public int computeCalories() {
        int sod=0;
        for(controller.MenuItem m: menuItemList)
        {
            sod=sod+m.computeCalories();
        }
        this.calories=sod;
        return sod;
    }

    @Override
    public float computeRating() {
        float sod=0;
        for(controller.MenuItem m: menuItemList)
        {
            sod=sod+m.computeRating();
        }
        sod=sod/menuItemList.size();
        this.rating=sod;
        return sod;
    }

    @Override
    public String computeName() {
        return this.name;
    }

    public List<controller.MenuItem> getMenuItemList() {
        return menuItemList;
    }

    public void setMenuItemList(List<controller.MenuItem> menuItemList) {
        this.menuItemList = menuItemList;
    }

    public void addBaseProd(MenuItem it)
    {
        menuItemList.add(it);
    }

}
