package controller;

import controller.ControllerAdmin;
import controller.ControllerClient;
import model.User;
import view.LoggingFrame;
import view.LoginFrame;
import view.RegisterFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.*;

public class ControllerLogin {
    private List<User> usersList=new ArrayList<User>();
    private LoginFrame fr1;
    private RegisterFrame registerFrame;
    private LoggingFrame logFr;
    private ControllerClient controllerClient;
    private ControllerAdmin controllerAdmin;
    private static User currentUser;

    public List<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }

    public LoginFrame getFr1() {
        return fr1;
    }

    public void setFr1(LoginFrame fr1) {
        this.fr1 = fr1;
    }

    public RegisterFrame getRegisterFrame() {
        return registerFrame;
    }

    public void setRegisterFrame(RegisterFrame registerFrame) {
        this.registerFrame = registerFrame;
    }

    public LoggingFrame getLogFr() {
        return logFr;
    }

    public void setLogFr(LoggingFrame logFr) {
        this.logFr = logFr;
    }



    public ControllerLogin() throws IOException {
       fr1 = new LoginFrame();
       registerFrame=new RegisterFrame();
       logFr=new LoggingFrame();


       User admin=new User("Alina@admin","12345", "admin");
       User employee=new User("Ionel@employee","mate333","employee");
       User client=new User("Georgi@client","giogio","client");
       usersList.add(admin);
       usersList.add(employee);
       usersList.add(client);

        fr1.setVisible(true);

        fr1.getLogin().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fr1.setVisible(false);
                logFr.setVisible(true);

                logFr.getOk().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try
                        {
                            int ok=1;
                            String user=logFr.getUsername().getText().toString();
                            String pass=logFr.getPassword().getText().toString();
                                System.out.println(user+" "+pass);
                            for(User us: usersList) {
                                if(us.getUsername().equals(user)&& us.getPassword().equals(pass) && us.getRole().equals("admin"))
                                    controllerAdmin=new ControllerAdmin();
                                else if(us.getUsername().equals(user)&& us.getPassword().equals(pass) && us.getRole().equals("client")) {
                                    controllerClient = new ControllerClient();
                                    currentUser=new User(us.getUsername(),us.getPassword(),"client");
                                }
                                else
                                {

                                }
                            }

                            logFr.setVisible(false);
                        }
                        catch(Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                });
            }
        });


        fr1.getRegister().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fr1.setVisible(false);
                registerFrame.setVisible(true);

                registerFrame.getOk().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try
                        {
                            int ok=1;
                            String user=registerFrame.getUsername().getText().toString();
                            String pass=registerFrame.getPassword().getText().toString();
                            User us=new User(user,pass,"client");

                            for(User s: usersList )
                                if(s.getUsername().equals(us.getUsername()) && s.getPassword().equals(us.getPassword())) ok=0;

                                if(ok==1) usersList.add(us);

                                for(User u: usersList)
                                System.out.println(u.getUsername()+" "+u.getPassword());
                                registerFrame.getUsername().setText("");
                                registerFrame.getPassword().setText("");
                                registerFrame.setVisible(false);
                                fr1.setVisible(true);
                        }
                        catch(Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                });
            }
        });
    }
    public static User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
