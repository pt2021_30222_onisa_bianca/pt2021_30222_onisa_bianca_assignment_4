package controller;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class FileWriterBill {
    private int nrBill;
    FileOutputStream file;

    public void wrFile(String res)
    {
        try
        {
            FileOutputStream oFile = new FileOutputStream("Bill"+nrBill+".txt");
            nrBill++;
            byte[] str= res.getBytes();
            oFile.write(str);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }


    public int getNrBill() {
        return nrBill;
    }

    public void setNrBill(int nrBill) {
        this.nrBill = nrBill;
    }
}
