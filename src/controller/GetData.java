package controller;

import model.BaseProduct;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class GetData {

    private static List<BaseProduct> listProd;

    public static void createProd(String sir) {
        String title;
        float rating = 0;
        int calories, protein, fat, sodium, price;

        String[] stringSplitArray = sir.split(",");
        title = stringSplitArray[0];
        try {
            rating = Float.parseFloat(stringSplitArray[1]);
        } catch (Exception e) {
           e.printStackTrace();
        }
        calories = Integer.parseInt(stringSplitArray[2]);
        protein = Integer.parseInt(stringSplitArray[3]);
        fat = Integer.parseInt(stringSplitArray[4]);
        sodium = Integer.parseInt(stringSplitArray[5]);
        price = Integer.parseInt(stringSplitArray[6]);

        BaseProduct product = new BaseProduct(title, rating, calories, protein, fat, sodium, price);
        for (BaseProduct prod : listProd) {
            if (prod.getTitle().equals(product.getTitle())) return;
        }
        listProd.add(product);
    }


    public static void getData() throws IOException {
        String fileName = "products.csv";
        listProd=new ArrayList<BaseProduct>();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(entry -> {
                String[] stringSplitArray = entry.split(",");
                try{
                    Float.parseFloat(stringSplitArray[1]);
                    createProd(entry);
                }catch(NumberFormatException e){
                }
                    });
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<BaseProduct> getListProd() {
        return listProd;
    }

    public static void setListProd(List<BaseProduct> listProd) {
        GetData.listProd = listProd;
    }
}
