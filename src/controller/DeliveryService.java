package controller;

import model.BaseProduct;
import model.Order;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class DeliveryService implements IDeliveryServiceProcessing {
    private HashSet<Order> orders= new HashSet<>();
    static int orderNo;

    @Override
    public void addMenuItem(String nume, float rating, int calories, int protein, int fat, int sodium, int price) throws IOException {
        List<List<String>> rows = Arrays.asList(
                Arrays.asList(nume, String.valueOf(rating),String.valueOf(calories),String.valueOf(protein),String.valueOf(fat),String.valueOf(sodium),String.valueOf(price)));

        FileWriter csvWriter = new FileWriter("products.csv", true);

        for (List<String> rowData : rows) {
            csvWriter.append(String.join(",", rowData));
            csvWriter.append("\n");
        }

        csvWriter.flush();
        csvWriter.close();
    }

    @Override
    public void addCompProd(List<MenuItem> lista, String nume, int price) throws IOException {
        float sumRating=0;
        int sumCal=0,sumProt=0, sumFat=0,sumSod=0;
        for(MenuItem item: lista)
        {

            sumRating=sumRating+item.computeRating();
            sumCal=sumCal+item.computeCalories();
            sumProt=sumProt+item.computeProtein();
            sumFat=sumFat+item.computeFat();
            sumSod=sumSod+item.computeSodium();

        }
        sumRating=sumRating/lista.size();

        List<List<String>> rows = Arrays.asList(
                Arrays.asList(nume, String.valueOf(sumRating),String.valueOf(sumCal),String.valueOf(sumProt),String.valueOf(sumFat),String.valueOf(sumSod),String.valueOf(price)));
        FileWriter csvWriter = new FileWriter("products.csv", true);
        for (List<String> rowData : rows) {
            csvWriter.append(String.join(",", rowData));
            csvWriter.append("\n");
        }

        csvWriter.flush();
        csvWriter.close();
    }


    @Override
    public void deleteProd(String prod) throws IOException {
        GetData gett=new GetData();
        gett.getData();
        List<BaseProduct> list=gett.getListProd();
        BaseProduct toRemove = null;
        for(BaseProduct produs:list)
        {
            if(produs.getTitle().trim().equals(prod)) toRemove=produs;
        }
        if(toRemove != null)
        {
            list.remove(toRemove);
        }

        FileWriter csvWriter = new FileWriter("products.csv");
        for (BaseProduct produs : list) {
            csvWriter.append(produs.getTitle() + "," + String.valueOf(produs.getRating())+","+String.valueOf(produs.getCalories())+","+String.valueOf(produs.getProtein())+","+String.valueOf(produs.getFat())+","+String.valueOf(produs.getSodium()+","+String.valueOf(produs.getPrice())));
            csvWriter.append("\n");
        }
        csvWriter.flush();
        csvWriter.close();
    }


    @Override
    public void updateProd(MenuItem prod, String name) throws IOException {
        GetData gett=new GetData();
        gett.getData();
        List<BaseProduct> list=gett.getListProd();
        BaseProduct toRemove = null;
        for(BaseProduct produs:list)
        {
            if(produs.getTitle().trim().equals(name))
            {
                toRemove=produs;
                break;
            }
        }
        if(toRemove != null)
        {
            list.remove(toRemove);
        }

        FileWriter csvWriter = new FileWriter("products.csv");
        for (BaseProduct produs : list) {
            csvWriter.append(produs.getTitle() + "," + String.valueOf(produs.getRating())+","+String.valueOf(produs.getCalories())+","+String.valueOf(produs.getProtein())+","+String.valueOf(produs.getFat())+","+String.valueOf(produs.getSodium()+","+String.valueOf(produs.getPrice())));
            csvWriter.append("\n");
        }

        csvWriter.append(prod.computeName() + "," + String.valueOf(prod.computeRating())+","+String.valueOf(prod.computeCalories())+","+String.valueOf(prod.computeProtein())+","+String.valueOf(prod.computeFat())+","+String.valueOf(prod.computeSodium())+","+String.valueOf(prod.computePrice()));
        csvWriter.flush();
        csvWriter.close();


    }

    @Override
    public void computeOrder(List<MenuItem> order) {
        orderNo++;
        Order ord=new Order();
        int sum=0;
        Date dNow = new Date( );
        String bill="";
            for(MenuItem item: order)
            {
                sum=sum+item.computePrice();
                bill=bill+item.computeName()+"   "+item.computePrice()+"\n";
            }
            bill=bill+"\n";
            bill=bill+"Total:          "+sum;
            FileWriterBill wr=new FileWriterBill();
            wr.wrFile(bill);

            ord.setID(orderNo);
            ord.setPrice(sum);
            ord.setClient(ControllerLogin.getCurrentUser());
            ord.setProducts(order);
            ord.setData(dNow);
            orders.add(ord);

            PrintOrders pn=new PrintOrders();
            String rez=pn.orderToString(ord);
            pn.printOrders(rez);
    }

    public HashSet<Order> getOrders() {
        return orders;
    }

    public void setOrders(HashSet<Order> orders) {
        this.orders = orders;
    }
}
