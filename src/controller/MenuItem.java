package controller;

public interface MenuItem {
    int computePrice();
    String print();
    int computeSodium();
    int computeFat();
    int computeProtein();
    int computeCalories();
    float computeRating();
    String computeName();
}
