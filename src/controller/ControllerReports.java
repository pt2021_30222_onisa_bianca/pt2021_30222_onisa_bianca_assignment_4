package controller;

import model.BaseProduct;
import model.Order;
import model.User;
import view.ReportsFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ControllerReports {
    public ControllerReports() throws FileNotFoundException {
        ReportsFrame fr = new ReportsFrame();
        fr.setVisible(true);

        ReportsWrite rw=new ReportsWrite();
        rw.readProd();
        List<Order> orders=rw.getOrders();                    //lista de orders din fisier
        Order ord=new Order();
        PrintOrders pr=new PrintOrders();

        List<BaseProduct> products=new ArrayList<>();           //pt rep2
        List<User> clients=new ArrayList<>();                  //pt rep3

        List<BaseProduct> prod4=new ArrayList<>();           //pt rep4

        fr.getGenB1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String rez="";
                String time1, time2;
                Date timeStart, timeEnd;
                time1 = fr.getStartHT().getText().toString();
                time2 = fr.getEndHT().getText().toString();
                List<Order> filteredList;
                try {
                    timeStart = new SimpleDateFormat("kk:mm:ss").parse(time1);
                    timeEnd = new SimpleDateFormat("kk:mm:ss").parse(time2);
                    filteredList = orders.stream().filter(c ->
                            (timeStart.getHours() <= c.getData().getHours()  && c.getData().getHours() < timeEnd.getHours()) ||
                                    (timeStart.getHours() <= c.getData().getHours()  && c.getData().getHours() <= timeEnd.getHours()
                                            && timeStart.getMinutes() <= c.getData().getMinutes() && c.getData().getMinutes() < timeEnd.getMinutes()) ||
                                    (timeStart.getHours() <= c.getData().getHours()  && c.getData().getHours() <= timeEnd.getHours()
                                            && timeStart.getMinutes() <= c.getData().getMinutes() && c.getData().getMinutes() <= timeEnd.getMinutes() &&
                                            timeStart.getSeconds() <= c.getData().getSeconds() && c.getData().getSeconds() < timeEnd.getSeconds()
                                    )).collect(Collectors.toList());
                    for (Order ord : filteredList)
                    {
                        PrintOrders rw=new PrintOrders();

                        rez = rez + "\n" + rw.orderToString(ord);
                    }
                    pr.printNedeed(rez, "Rep1.txt");
                } catch (ParseException parseException) {
                    parseException.printStackTrace();
                }
            }
        });

        fr.getGenB2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String rez = "";

                for (Order order : orders)
                {
                    for(MenuItem prod: order.getProducts())
                    {
                        products.add(new BaseProduct(prod.computeName().trim(), 0.0f, 1,1,1,1,1));
                    }
                }
                Map<String, Long> counts =
                        products.stream().collect(Collectors.groupingBy(c -> c.getTitle(), Collectors.counting()));

                for (Map.Entry<String, Long> entry : counts.entrySet())
                {
                   if ( entry.getValue() >= Integer.parseInt(fr.getNrTimesT().getText().toString()))
                   {
                       rez = rez + "\n" + entry.getKey() + " " + entry.getValue();
                   }
                }
                pr.printNedeed(rez,"Rep2.txt");
                products.clear();
            }
        });

        fr.getGenB3().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String rez = "";
                List<Order> filteredList;
                filteredList = orders.stream().filter(c -> c.getPrice() > Integer.parseInt(fr.getHighPriceT().getText().toString())).collect(Collectors.toList());
                Map<String, Long> counts =
                        filteredList.stream().collect(Collectors.groupingBy(c -> c.getClient().getUsername(), Collectors.counting()));

                for (Map.Entry<String, Long> entry : counts.entrySet())
                {
                    if ( entry.getValue() >= Integer.parseInt(fr.getClTimesT().getText().toString()))
                    {
                        rez = rez + "\n" + entry.getKey() + " " + entry.getValue();
                    }
                }
                pr.printNedeed(rez,"Rep3.txt");
            }
        });

        fr.getGenB4().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String rez = "";
                String time1;
                Date timeDate;
                List<Order> filteredList;
                time1 = fr.getSpecifiedDayT().getText().toString();
                try {
                    timeDate = new SimpleDateFormat("dd-MM-yyyy").parse(time1);
                    filteredList = orders.stream().filter(c -> c.getData().getDay() == timeDate.getDay() && c.getData().getMonth() == timeDate.getMonth() &&
                            c.getData().getYear() == timeDate.getYear()).collect(Collectors.toList());

                    for (Order order : filteredList)
                    {
                        for(MenuItem prod: order.getProducts())
                        {
                            products.add(new BaseProduct(prod.computeName().trim(), 0.0f, 1,1,1,1,1));
                        }
                    }
                    Map<String, Long> counts =
                            products.stream().collect(Collectors.groupingBy(c -> c.getTitle(), Collectors.counting()));

                    for (Map.Entry<String, Long> entry : counts.entrySet())
                    {
                            rez = rez + "\n" + entry.getKey() + " " + entry.getValue();
                    }
                    pr.printNedeed(rez,"Rep4.txt");
                    products.clear();

                } catch (ParseException parseException) {
                    parseException.printStackTrace();
                }
            }
        });
    }
}
