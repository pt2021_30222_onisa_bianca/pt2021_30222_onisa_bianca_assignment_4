package controller;

import model.BaseProduct;
import model.Order;
import view.ClientFrame;
import view.OrderAddedFrame;
import view.ProdAddedFrame;
import view.SearchFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ControllerClient {

    private ClientFrame clFr;
    private ControllerLogin login;
    private SearchFrame sFr;
    private Order order=new Order();

    public ControllerClient() throws IOException {
        clFr=new ClientFrame();
        clFr.setVisible(true);


        clFr.getLogout().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clFr.setVisible(false);
                try {
                    login=new ControllerLogin();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });

        clFr.getSearchB().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String rez=clFr.getSearchT().getText().toString();
                String combo=clFr.getCombo().getSelectedItem().toString();

                GetData gett=new GetData();
                try {
                    gett.getData();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                List<BaseProduct> list=gett.getListProd();
                try {
                    sFr=new SearchFrame(rez,combo);
                    sFr.setVisible(true);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }


            }
        });

        clFr.getAddToOrder().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                GetData data=new GetData();
                try {
                    data.getData();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                String produs=clFr.getProdText().getText().toString();
                List<BaseProduct> toateProd=new ArrayList<BaseProduct>();
                toateProd= data.getListProd();

                for(BaseProduct prod: toateProd)
                {
                    if (prod.getTitle().trim().equals(produs))
                    {
                        order.getProducts().add(prod);
                        ProdAddedFrame fr = new ProdAddedFrame();
                    }
                }
            }
        });

        clFr.getPlaceOrder().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DeliveryService del=new DeliveryService();
                del.computeOrder(order.getProducts());
                OrderAddedFrame fr=new OrderAddedFrame();
                order.setProducts(new ArrayList<>());
            }
        });
    }
}
