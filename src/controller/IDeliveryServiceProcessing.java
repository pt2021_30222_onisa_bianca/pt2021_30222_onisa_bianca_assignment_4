package controller;

import java.io.IOException;
import java.util.List;

public interface IDeliveryServiceProcessing {
    public void addMenuItem(String nume, float rating, int calories, int protein, int fat, int sodium, int price) throws IOException;
    public void addCompProd(List<MenuItem> lista, String nume, int price) throws IOException;
    public void deleteProd(String prod) throws IOException;
    public void updateProd(MenuItem prod, String name) throws IOException;
    public void computeOrder(List<MenuItem> order);
}
