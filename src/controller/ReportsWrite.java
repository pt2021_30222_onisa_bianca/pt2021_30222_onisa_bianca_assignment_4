package controller;

import model.BaseProduct;
import model.Order;
import model.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class ReportsWrite {
    private List<Order> orders=new ArrayList<>();

    public void stringToOrder(String str) throws ParseException {
        Order ord=new Order();
        String[] stringSplitArray = str.split(",");
        ord.setID(Integer.parseInt(stringSplitArray[0]));
        ord.setClient(new User(stringSplitArray[1],"",""));
        ord.setPrice(Integer.parseInt(stringSplitArray[2]));

        SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy kk:mm:ss");
        Date date = dt.parse(stringSplitArray[3]);
        ord.setData(date);

        String[] products=stringSplitArray[4].trim().split(";");
        int i=0;
        while(i<products.length)
        {
            ord.getProducts().add(new BaseProduct(products[i]));
            i++;
        }
        orders.add(ord);
    }

    public void readProd() throws FileNotFoundException {
        try
        {
            FileInputStream fis=new FileInputStream("Orders.txt");
            Scanner sc=new Scanner(fis);
            while(sc.hasNextLine())
            {
               stringToOrder(sc.nextLine());
            }
            sc.close();
        }
        catch(IOException | ParseException e)
        {
            e.printStackTrace();
        }
    }


    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
