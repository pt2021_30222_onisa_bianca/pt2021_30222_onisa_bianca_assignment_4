package controller;

import model.Order;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;

public class PrintOrders {
    FileOutputStream file;

    public void printOrders(String res)
    {
        try
        {
            FileOutputStream oFile = new FileOutputStream("Orders.txt",true);
            byte[] str= res.getBytes();
            oFile.write(str);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void printNedeed(String res, String file)
    {
        try
        {
            FileOutputStream oFile = new FileOutputStream(file);
            byte[] str= res.getBytes();
            oFile.write(str);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public String orderToString (Order order)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String date = sdf.format(order.getData());
        String rez=String.valueOf(order.getID())+","+order.getClient().getUsername()+","+order.getPrice()+","+date+","+order.enumProd()+"\n";
        return rez;
    }

}
