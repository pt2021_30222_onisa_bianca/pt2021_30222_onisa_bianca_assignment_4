package controller;

import model.BaseProduct;
import view.AdminFrame;
import view.ProdAddedFrame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ControllerAdmin {
    AdminFrame clFr;
    ControllerLogin login;
    DeliveryService delServ;
    List<MenuItem> menu;
    String produse="";
    public ControllerAdmin() throws IOException {
        clFr=new AdminFrame();
        clFr.setVisible(true);
        menu=new ArrayList<>();

        clFr.getLogout().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clFr.setVisible(false);
                try {
                    login=new ControllerLogin();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });

        clFr.getAddBaseProd().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                delServ=new DeliveryService();
                String title=clFr.getTitleT().getText().toString();
                float rating=Float.parseFloat(clFr.getRatingT().getText().toString());
                int calories=Integer.parseInt(clFr.getCaloriesT().getText().toString());
                int protein=Integer.parseInt(clFr.getProteinT().getText().toString());
                int fat=Integer.parseInt(clFr.getFatT().getText().toString());
                int sodium=Integer.parseInt(clFr.getSodiumT().getText().toString());
                int price=Integer.parseInt(clFr.getPriceT().getText().toString());
                try {
                    delServ.addMenuItem(title,rating,calories,protein,fat,sodium,price);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });

        clFr.getDeleteProd().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                delServ=new DeliveryService();
                String title=clFr.getTitleT().getText().toString();
                try {
                    delServ.deleteProd(title);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });

        clFr.getUpdateProd().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                delServ=new DeliveryService();
                String title=clFr.getTitleT().getText().toString();
                float rating=Float.parseFloat(clFr.getRatingT().getText().toString());
                int calories=Integer.parseInt(clFr.getCaloriesT().getText().toString());
                int protein=Integer.parseInt(clFr.getProteinT().getText().toString());
                int fat=Integer.parseInt(clFr.getFatT().getText().toString());
                int sodium=Integer.parseInt(clFr.getSodiumT().getText().toString());
                int price=Integer.parseInt(clFr.getPriceT().getText().toString());
                java.awt.MenuItem prod=new BaseProduct(title,rating,calories,protein,fat,sodium,price);
                try {
                    delServ.updateProd((MenuItem) prod,title);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });

        clFr.getAddToMenu().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String title=clFr.getTitleT().getText().toString();
                GetData gett=new GetData();
                try {
                    GetData.getData();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }

                List<BaseProduct> list=gett.getListProd();
                BaseProduct toAdd = null;
                for(BaseProduct produs:list)
                {
                    if(produs.getTitle().trim().equals(title)) toAdd=produs;
                }
                if(toAdd != null)
                {
                    menu.add(toAdd);
                    produse=produse+"    "+toAdd.getTitle();
                    ProdAddedFrame fr = new ProdAddedFrame();
                }
            }
        });

        clFr.getAddMenu().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nume=clFr.getNumeT().getText().toString()+" ("+produse+" )";
                int pret=Integer.parseInt(clFr.getPretT().getText().toString());
                delServ=new DeliveryService();
                try {
                    delServ.addCompProd(menu,nume,pret);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });

        clFr.getGenerateRap().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ControllerReports cr=new ControllerReports();
                } catch (FileNotFoundException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                }
            }
        });
    }
}
