package controller;

import model.CompositeProduct;

import java.io.*;

public class Serializator {
    private FileInputStream file;
    private ObjectInputStream in;
    private CompositeProduct prod;
    private String filename;

    public CompositeProduct readProd()
    {
      try
      {
          file=new FileInputStream(filename);
          in=new ObjectInputStream(file);
          prod=(CompositeProduct)in.readObject();
          in.close();
          file.close();
      }
      catch(Exception ex)
      {
          ex.printStackTrace();
      }
        return prod;
    }

    public void writeProd(CompositeProduct prod)
    {
        try
        {
            FileOutputStream fileO=new FileOutputStream(filename);
            ObjectOutputStream out= new ObjectOutputStream(fileO);
            out.writeObject(prod);
            out.close();
            fileO.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

}
