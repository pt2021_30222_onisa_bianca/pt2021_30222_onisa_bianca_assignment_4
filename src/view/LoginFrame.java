package view;

import javax.swing.*;
import java.awt.*;

public class LoginFrame extends JFrame {
    private JButton login;
    private JButton register;
    private JLabel title;

   public LoginFrame()
   {
       this.setLayout(null);
       this.setBounds(550, 150, 500, 500);
       this.getContentPane().setBackground(new Color(179, 255, 217));
       this.setTitle("Log In");
       this.setDefaultCloseOperation(EXIT_ON_CLOSE);

       title=new JLabel("Food Delivery");
       title.setFont(new Font("Georgia", Font.BOLD,35));
       title.setBounds(120,30,300,70);
       this.add(title);

       login=new JButton("Log In");
       login.setFont(new Font("Georgia", Font.BOLD,25));
       login.setBounds(165,120,150,70);
       login.setBackground(new Color(0, 102, 255));
       this.add(login);

       register=new JButton("Register");
       register.setFont(new Font("Georgia", Font.BOLD,25));
       register.setBounds(165,240,150,70);
       register.setBackground(new Color(0, 102, 255));
       this.add(register);
   }

    public JButton getLogin() {
        return login;
    }

    public void setLogin(JButton login) {
        this.login = login;
    }

    public JButton getRegister() {
        return register;
    }

    public void setRegister(JButton register) {
        this.register = register;
    }
}
