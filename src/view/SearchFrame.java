package view;

import controller.GetData;
import model.BaseProduct;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Character.isLetter;

public class SearchFrame extends JFrame {

    private DefaultTableModel table;
    private String[] columnNames= new String[]{"Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price"};
    private JTable tabelB;

    public SearchFrame(String cauta, String combo) throws IOException {
        this.setLayout(null);
        this.setBounds(250, 200, 1100, 500);
        this.getContentPane().setBackground(new Color(51, 204, 255));
        this.setTitle("Search Result");

        tabelB=new JTable();
        tabelB=searchRes(cauta, combo);
        JScrollPane js = new JScrollPane(tabelB);
        tabelB.setDefaultEditor(Object.class,null);
        js.setBounds(50,20,1000,400);
        this.add(js);
    }

    public JTable searchRes(String cauta, String combo) throws IOException {
        GetData gett=new GetData();
        gett.getData();
        List<BaseProduct> list=gett.getListProd();
        JTable tab;
        List<BaseProduct> filteredList;

        table= new DefaultTableModel();
        table.setColumnIdentifiers(columnNames);

        if(combo.equals("Title")) {
            filteredList = list.stream().filter(c -> c.getTitle().contains(cauta)).collect(Collectors.toList());
        }
        else if (combo.equals("Rating"))
        {
            filteredList = list.stream().filter(c -> c.getRating()==(Float.parseFloat(cauta))).collect(Collectors.toList());
        }
        else if (combo.equals("Calories"))
        {
            filteredList = list.stream().filter(c -> c.getCalories()==(Integer.parseInt(cauta))).collect(Collectors.toList());
        }
        else if (combo.equals("Protein"))
        {
            filteredList = list.stream().filter(c -> c.getProtein()==(Integer.parseInt(cauta))).collect(Collectors.toList());
        }
        else if (combo.equals("Fat"))
        {
            filteredList = list.stream().filter(c -> c.getFat()==(Integer.parseInt(cauta))).collect(Collectors.toList());
        }
        else if (combo.equals("Sodium"))
        {
            filteredList = list.stream().filter(c -> c.getSodium()==(Integer.parseInt(cauta))).collect(Collectors.toList());
        }
        else
        {
            filteredList = list.stream().filter(c -> c.getPrice()==(Integer.parseInt(cauta))).collect(Collectors.toList());
        }

        for(BaseProduct prod: filteredList) {
                Object[] rez = new Object[7];
                rez[0] = prod.getTitle();
                rez[1] = prod.getRating();
                rez[2] = prod.getCalories();
                rez[3] = prod.getProtein();
                rez[4] = prod.getFat();
                rez[5] = prod.getSodium();
                rez[6] = prod.getPrice();
                table.addRow(rez);
        }
        tab=new JTable(table);
        return(tab);

    }

}
