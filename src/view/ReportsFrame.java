package view;

import javax.swing.*;
import java.awt.*;

public class ReportsFrame extends JFrame {

    private JLabel startHL;
    private JLabel endHL;
    private JTextField startHT;
    private JTextField endHT;
    private JButton genB1;
    private JLabel nrTimes;
    private JTextField nrTimesT;
    private JButton genB2;
    private JLabel iar;
    private JTextField clTimesT;
    private JLabel highPrice;
    private JTextField highPriceT;
    private JButton genB3;
    private JTextField specifiedDayT;
    private JLabel specDay;
    private JButton genB4;

    public ReportsFrame() {
        this.setLayout(null);
        this.setBounds(200, 100, 800, 670);
        this.getContentPane().setBackground(new Color(180, 100, 150));
        this.setTitle("Generate Reports");

        startHL=new JLabel("Start hour:");
        startHL.setFont(new Font("Georgia", Font.BOLD,18));
        startHL.setBounds(30, 20,150,50);
        this.add(startHL);

        endHL=new JLabel("End hour:");
        endHL.setFont(new Font("Georgia", Font.BOLD,18));
        endHL.setBounds(30, 70,150,50);
        this.add(endHL);

        startHT=new JTextField();
        startHT.setBounds(190,30,150,30);
        this.add(startHT);

        endHT=new JTextField();
        endHT.setBounds(190,80,150,30);
        this.add(endHT);

        genB1=new JButton("Generate orders between hours");
        genB1.setFont(new Font("Georgia", Font.BOLD,13));
        genB1.setBackground(new Color(100,200,170));
        genB1.setBounds(420,40,270,50);
        this.add(genB1);

        nrTimes=new JLabel("Products ordered more than n times:");
        nrTimes.setFont(new Font("Georgia", Font.BOLD,17));
        nrTimes.setBounds(30, 150,400,50);
        this.add(nrTimes);

        nrTimesT=new JTextField();
        nrTimesT.setBounds(80,200,170,30);
        this.add(nrTimesT);

        genB2=new JButton("Generate ordered products");
        genB2.setFont(new Font("Georgia", Font.BOLD,13));
        genB2.setBackground(new Color(100,200,170));
        genB2.setBounds(420,180,270,50);
        this.add(genB2);

        iar=new JLabel("Clients that ordered more than n times:");
        iar.setFont(new Font("Georgia", Font.BOLD,17));
        iar.setBounds(30, 270,400,50);
        this.add(iar);

        clTimesT=new JTextField();
        clTimesT.setBounds(80,320,170,30);
        this.add(clTimesT);

        highPrice=new JLabel("And the price was higher than:");
        highPrice.setFont(new Font("Georgia", Font.BOLD,17));
        highPrice.setBounds(30, 360,400,50);
        this.add(highPrice);

        highPriceT=new JTextField();
        highPriceT.setBounds(80,410,170,30);
        this.add(highPriceT);

        genB3=new JButton("Generate specified clients");
        genB3.setFont(new Font("Georgia", Font.BOLD,13));
        genB3.setBackground(new Color(100,200,170));
        genB3.setBounds(420,350,270,50);
        this.add(genB3);

        specDay=new JLabel("Products ordered within the specified day:");
        specDay.setFont(new Font("Georgia", Font.BOLD,17));
        specDay.setBounds(30, 480,400,50);
        this.add(specDay);

        specifiedDayT=new JTextField();
        specifiedDayT.setBounds(80,530,170,30);
        this.add(specifiedDayT);

        genB4=new JButton("Generate products");
        genB4.setFont(new Font("Georgia", Font.BOLD,13));
        genB4.setBackground(new Color(100,200,170));
        genB4.setBounds(420,500,270,50);
        this.add(genB4);
    }

    public JTextField getStartHT() {
        return startHT;
    }

    public void setStartHT(JTextField startHT) {
        this.startHT = startHT;
    }

    public JTextField getEndHT() {
        return endHT;
    }

    public void setEndHT(JTextField endHT) {
        this.endHT = endHT;
    }

    public JButton getGenB1() {
        return genB1;
    }

    public void setGenB1(JButton genB1) {
        this.genB1 = genB1;
    }

    public JTextField getNrTimesT() {
        return nrTimesT;
    }

    public void setNrTimesT(JTextField nrTimesT) {
        this.nrTimesT = nrTimesT;
    }

    public JButton getGenB2() {
        return genB2;
    }

    public void setGenB2(JButton genB2) {
        this.genB2 = genB2;
    }

    public JTextField getClTimesT() {
        return clTimesT;
    }

    public void setClTimesT(JTextField clTimesT) {
        this.clTimesT = clTimesT;
    }

    public JTextField getHighPriceT() {
        return highPriceT;
    }

    public void setHighPriceT(JTextField highPriceT) {
        this.highPriceT = highPriceT;
    }

    public JButton getGenB3() {
        return genB3;
    }

    public void setGenB3(JButton genB3) {
        this.genB3 = genB3;
    }

    public JTextField getSpecifiedDayT() {
        return specifiedDayT;
    }

    public void setSpecifiedDayT(JTextField specifiedDayT) {
        this.specifiedDayT = specifiedDayT;
    }

    public JButton getGenB4() {
        return genB4;
    }

    public void setGenB4(JButton genB4) {
        this.genB4 = genB4;
    }
}
