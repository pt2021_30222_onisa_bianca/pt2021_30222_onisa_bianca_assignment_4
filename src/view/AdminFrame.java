package view;

import controller.GetData;
import model.BaseProduct;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.IOException;
import java.util.List;

public class AdminFrame extends JFrame {

    private DefaultTableModel table;
    private String[] columnNames= new String[]{"Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price"};
    private JTable tabelB;
    private JLabel menu;
    private JButton logout;
    private JLabel titleL;
    private JLabel ratingL;
    private JLabel caloriesL;
    private JLabel proteinL;
    private JLabel fatL;
    private JLabel sodiumL;
    private JLabel priceL;
    private JTextField titleT;
    private JTextField ratingT;
    private JTextField caloriesT;
    private JTextField proteinT;
    private JTextField fatT;
    private JTextField sodiumT;
    private JTextField priceT;
    private JButton addBaseProd;
    private JButton deleteProd;
    private JButton updateProd;
    private JButton addToMenu;
    private JButton addMenu;
    private JLabel numeL;
    private JLabel pretL;
    private JTextField numeT;
    private JTextField pretT;
    private JButton generateRap;


    public AdminFrame() throws IOException {
        this.setLayout(null);
        this.setBounds(100, 50, 1400, 800);
        this.getContentPane().setBackground(new Color(179, 255, 217));
        this.setTitle("Administrator");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        GetData gett=new GetData();
        gett.getData();
        List<BaseProduct> list=gett.getListProd();

        table= new DefaultTableModel();
        table.setColumnIdentifiers(columnNames);

        for(BaseProduct prod: list)
        {
            Object[] rez=new Object[7];
            rez[0]=prod.getTitle();
            rez[1]=prod.getRating();
            rez[2]=prod.getCalories();
            rez[3]=prod.getProtein();
            rez[4]=prod.getFat();
            rez[5]=prod.getSodium();
            rez[6]=prod.getPrice();
            table.addRow(rez);
        }

        tabelB=new JTable(table);
        JScrollPane js = new JScrollPane(tabelB);
        tabelB.setDefaultEditor(Object.class,null);
        js.setBounds(200,400,1000,300);
        this.add(js);

        menu=new JLabel("Menu:");
        menu.setFont(new Font("Georgia", Font.BOLD,25));
        menu.setBounds(230, 330,150,50);
        this.add(menu);

        logout=new JButton("Log out");
        logout.setFont(new Font("Georgia", Font.BOLD,23));
        logout.setBackground(new Color(147,70,200));
        logout.setBounds(1200,50,150,50);
        this.add(logout);

        titleL=new JLabel("Title:");
        titleL.setFont(new Font("Georgia", Font.BOLD,20));
        titleL.setBounds(30, 30,150,50);
        this.add(titleL);

        ratingL=new JLabel("Rating:");
        ratingL.setFont(new Font("Georgia", Font.BOLD,20));
        ratingL.setBounds(30, 60,150,50);
        this.add(ratingL);

        caloriesL=new JLabel("Calories:");
        caloriesL.setFont(new Font("Georgia", Font.BOLD,20));
        caloriesL.setBounds(30, 90,150,50);
        this.add(caloriesL);

        proteinL=new JLabel("Protein:");
        proteinL.setFont(new Font("Georgia", Font.BOLD,20));
        proteinL.setBounds(30, 120,150,50);
        this.add(proteinL);

        fatL=new JLabel("Fat:");
        fatL.setFont(new Font("Georgia", Font.BOLD,20));
        fatL.setBounds(30, 150,150,50);
        this.add(fatL);

        sodiumL=new JLabel("Sodium:");
        sodiumL.setFont(new Font("Georgia", Font.BOLD,20));
        sodiumL.setBounds(30, 180,150,50);
        this.add(sodiumL);

        priceL=new JLabel("Price:");
        priceL.setFont(new Font("Georgia", Font.BOLD,20));
        priceL.setBounds(30, 210,150,50);
        this.add(priceL);

        titleT=new JTextField();
        titleT.setBounds(150,40,150,30);
        this.add(titleT);

        ratingT=new JTextField();
        ratingT.setBounds(150,70,150,30);
        this.add(ratingT);

        caloriesT=new JTextField();
        caloriesT.setBounds(150,100,150,30);
        this.add(caloriesT);

        proteinT=new JTextField();
        proteinT.setBounds(150,130,150,30);
        this.add(proteinT);

        fatT=new JTextField();
        fatT.setBounds(150,160,150,30);
        this.add(fatT);

        sodiumT=new JTextField();
        sodiumT.setBounds(150,190,150,30);
        this.add(sodiumT);

        priceT=new JTextField();
        priceT.setBounds(150,220,150,30);
        this.add(priceT);

        addBaseProd=new JButton("Add Product");
        addBaseProd.setFont(new Font("Georgia", Font.BOLD,12));
        addBaseProd.setBackground(new Color(100,200,60));
        addBaseProd.setBounds(320,80,150,50);
        this.add(addBaseProd);

        deleteProd=new JButton("Delete Product");
        deleteProd.setFont(new Font("Georgia", Font.BOLD,12));
        deleteProd.setBackground(new Color(100,200,60));
        deleteProd.setBounds(320,130,150,50);
        this.add(deleteProd);

        updateProd=new JButton("Update Product");
        updateProd.setFont(new Font("Georgia", Font.BOLD,12));
        updateProd.setBackground(new Color(100,200,60));
        updateProd.setBounds(320,180,150,50);
        this.add(updateProd);

        addToMenu=new JButton("Add to Menu");
        addToMenu.setFont(new Font("Georgia", Font.BOLD,12));
        addToMenu.setBackground(new Color(100,200,60));
        addToMenu.setBounds(950,100,150,50);
        this.add(addToMenu);

        addMenu=new JButton("Add New Menu");
        addMenu.setFont(new Font("Georgia", Font.BOLD,12));
        addMenu.setBackground(new Color(100,200,60));
        addMenu.setBounds(950,160,150,50);
        this.add(addMenu);

        numeL=new JLabel("Menu Name:");
        numeL.setFont(new Font("Georgia", Font.BOLD,20));
        numeL.setBounds(600, 110,150,50);
        this.add(numeL);

        pretL=new JLabel("Menu Price:");
        pretL.setFont(new Font("Georgia", Font.BOLD,20));
        pretL.setBounds(600, 150,150,50);
        this.add(pretL);

        numeT=new JTextField();
        numeT.setBounds(750,120,150,30);
        this.add(numeT);

        pretT=new JTextField();
        pretT.setBounds(750,160,150,30);
        this.add(pretT);

        generateRap=new JButton("Generate Reports");
        generateRap.setFont(new Font("Georgia", Font.BOLD,17));
        generateRap.setBackground(new Color(100,0,160));
        generateRap.setBounds(700,300,220,50);
        this.add(generateRap);
    }

    public JButton getLogout() {
        return logout;
    }

    public void setLogout(JButton logout) {
        this.logout = logout;
    }

    public JTextField getTitleT() {
        return titleT;
    }

    public void setTitleT(JTextField titleT) {
        this.titleT = titleT;
    }

    public JTextField getRatingT() {
        return ratingT;
    }

    public void setRatingT(JTextField ratingT) {
        this.ratingT = ratingT;
    }

    public JTextField getCaloriesT() {
        return caloriesT;
    }

    public void setCaloriesT(JTextField caloriesT) {
        this.caloriesT = caloriesT;
    }

    public JTextField getProteinT() {
        return proteinT;
    }

    public void setProteinT(JTextField proteinT) {
        this.proteinT = proteinT;
    }

    public JTextField getFatT() {
        return fatT;
    }

    public void setFatT(JTextField fatT) {
        this.fatT = fatT;
    }

    public JTextField getSodiumT() {
        return sodiumT;
    }

    public void setSodiumT(JTextField sodiumT) {
        this.sodiumT = sodiumT;
    }

    public JTextField getPriceT() {
        return priceT;
    }

    public void setPriceT(JTextField priceT) {
        this.priceT = priceT;
    }

    public JButton getAddBaseProd() {
        return addBaseProd;
    }

    public void setAddBaseProd(JButton addBaseProd) {
        this.addBaseProd = addBaseProd;
    }

    public JButton getDeleteProd() {
        return deleteProd;
    }

    public void setDeleteProd(JButton deleteProd) {
        this.deleteProd = deleteProd;
    }

    public JButton getUpdateProd() {
        return updateProd;
    }

    public void setUpdateProd(JButton updateProd) {
        this.updateProd = updateProd;
    }

    public JButton getAddToMenu() {
        return addToMenu;
    }

    public void setAddToMenu(JButton addToMenu) {
        this.addToMenu = addToMenu;
    }

    public JButton getAddMenu() {
        return addMenu;
    }

    public void setAddMenu(JButton addMenu) {
        this.addMenu = addMenu;
    }

    public JTextField getNumeT() {
        return numeT;
    }

    public void setNumeT(JTextField numeT) {
        this.numeT = numeT;
    }

    public JTextField getPretT() {
        return pretT;
    }

    public void setPretT(JTextField pretT) {
        this.pretT = pretT;
    }

    public JButton getGenerateRap() {
        return generateRap;
    }

    public void setGenerateRap(JButton generateRap) {
        this.generateRap = generateRap;
    }
}
