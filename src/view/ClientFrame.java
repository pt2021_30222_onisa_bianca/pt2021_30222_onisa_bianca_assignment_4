package view;

import controller.GetData;
import model.BaseProduct;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.IOException;
import java.util.List;

public class ClientFrame extends JFrame {

    private DefaultTableModel table;
    private String[] columnNames= new String[]{"Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price"};
    private JTable tabelB;
    private JLabel menu;
    private JButton logout;
    private JTextField searchT;
    private JButton searchB;
    private JComboBox<String> combo;
    private JTextField prodText;
    private JLabel product;
    private JButton addToOrder;
    private JButton placeOrder;

    public ClientFrame() throws IOException {
        this.setLayout(null);
        this.setBounds(100, 50, 1400, 800);
        this.getContentPane().setBackground(new Color(179, 255, 217));
        this.setTitle("Client");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        GetData gett=new GetData();
        gett.getData();
        List<BaseProduct> list=gett.getListProd();

        table= new DefaultTableModel();
        table.setColumnIdentifiers(columnNames);

        for(BaseProduct prod: list)
        {
            Object[] rez=new Object[7];
            rez[0]=prod.getTitle();
            rez[1]=prod.getRating();
            rez[2]=prod.getCalories();
            rez[3]=prod.getProtein();
            rez[4]=prod.getFat();
            rez[5]=prod.getSodium();
            rez[6]=prod.getPrice();
            table.addRow(rez);
        }

        tabelB=new JTable(table);
        JScrollPane js = new JScrollPane(tabelB);
        tabelB.setDefaultEditor(Object.class,null);
        js.setBounds(200,400,1000,300);
        this.add(js);

        menu=new JLabel("Menu:");
        menu.setFont(new Font("Georgia", Font.BOLD,25));
        menu.setBounds(230, 330,150,50);
        this.add(menu);

        logout=new JButton("Log out");
        logout.setFont(new Font("Georgia", Font.BOLD,23));
        logout.setBackground(new Color(147,70,200));
        logout.setBounds(1200,50,150,50);
        this.add(logout);

        searchT=new JTextField();
        searchT.setBounds(700,320,200,40);
        this.add(searchT);

        prodText=new JTextField();
        prodText.setBounds(200,150,250,40);
        this.add(prodText);

        product=new JLabel("Choose product:");
        product.setFont(new Font("Georgia", Font.BOLD,18));
        product.setBounds(200, 100,170,50);
        this.add(product);

        addToOrder=new JButton("Add to order");
        addToOrder.setFont(new Font("Georgia", Font.BOLD,20));
        addToOrder.setBackground(new Color(255, 255, 0));
        addToOrder.setBounds(570,150,200,50);
        this.add(addToOrder);

        placeOrder=new JButton("Place order");
        placeOrder.setFont(new Font("Georgia", Font.BOLD,20));
        placeOrder.setBackground(new Color(204, 204, 0));
        placeOrder.setBounds(800,150,200,50);
        this.add(placeOrder);

        searchB=new JButton("Search");
        searchB.setFont(new Font("Georgia", Font.BOLD,23));
        searchB.setBackground(new Color(100,200,60));
        searchB.setBounds(950,315,150,50);
        this.add(searchB);

        combo=new JComboBox<String>();
        combo.setBounds(500,320,150,40);
        combo.setFont(new Font("Georgia", Font.BOLD,18));
        combo.addItem("Title");
        combo.addItem("Rating");
        combo.addItem("Calories");
        combo.addItem("Protein");
        combo.addItem("Fat");
        combo.addItem("Sodium");
        combo.addItem("Price");
        this.add(combo);
    }

    public DefaultTableModel getTable() {
        return table;
    }

    public void setTable(DefaultTableModel table) {
        this.table = table;
    }

    public JButton getLogout() {
        return logout;
    }

    public void setLogout(JButton logout) {
        this.logout = logout;
    }

    public JButton getSearchB() {
        return searchB;
    }

    public void setSearchB(JButton searchB) {
        this.searchB = searchB;
    }

    public JTextField getSearchT() {
        return searchT;
    }

    public void setSearchT(JTextField searchT) {
        this.searchT = searchT;
    }

    public JComboBox<String> getCombo() {
        return combo;
    }

    public void setCombo(JComboBox<String> combo) {
        this.combo = combo;
    }

    public JTextField getProdText() {
        return prodText;
    }

    public void setProdText(JTextField prodText) {
        this.prodText = prodText;
    }

    public JButton getAddToOrder() {
        return addToOrder;
    }

    public void setAddToOrder(JButton addToOrder) {
        this.addToOrder = addToOrder;
    }

    public JButton getPlaceOrder() {
        return placeOrder;
    }

    public void setPlaceOrder(JButton placeOrder) {
        this.placeOrder = placeOrder;
    }
}
