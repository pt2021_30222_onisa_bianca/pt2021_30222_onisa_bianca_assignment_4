package view;

import javax.swing.*;
import java.awt.*;

public class OrderAddedFrame extends JFrame {
    private JLabel lab;
    public OrderAddedFrame() {
        this.setLayout(null);
        this.setBounds(600, 300, 400, 200);
        this.getContentPane().setBackground(new Color(179, 255, 217));
        this.setTitle("Product added");

        lab = new JLabel("Order added!");
        lab.setFont(new Font("Georgia", Font.BOLD, 30));
        lab.setBounds(65, 28, 300, 100);
        this.add(lab);
        this.setVisible(true);
    }
}
