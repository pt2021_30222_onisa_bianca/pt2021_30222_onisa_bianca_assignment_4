package view;

import javax.swing.*;
import java.awt.*;

public class RegisterFrame extends JFrame {

     private JTextField username;
     private JTextField password;
     private JLabel luser;
     private JLabel lpass;
     private JButton ok;
     private JLabel reg;

    public RegisterFrame() {
        this.setLayout(null);
        this.setBounds(550, 150, 500, 500);
        this.getContentPane().setBackground(new Color(179, 255, 217));
        this.setTitle("Register");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        ok=new JButton("Confirm");
        ok.setFont(new Font("Georgia", Font.BOLD,22));
        ok.setBounds(165,320,140,60);
        ok.setBackground(new Color(150, 102, 255));
        this.add(ok);

        reg=new JLabel("Register");
        reg.setFont(new Font("Georgia", Font.BOLD,30));
        reg.setBounds(175,20,150,70);
        this.add(reg);

        luser=new JLabel("Username:");
        luser.setFont(new Font("Georgia", Font.BOLD,23));
        luser.setBounds(30,120,150,70);
        this.add(luser);

        lpass=new JLabel("Password:");
        lpass.setFont(new Font("Georgia", Font.BOLD,23));
        lpass.setBounds(30,200,150,70);
        this.add(lpass);

        username=new JTextField();
        username.setBounds(200,140,200,40);
        this.add(username);

        password=new JTextField();
        password.setBounds(200,220,200,40);
        this.add(password);

    }

    public JTextField getUsername() {
        return username;
    }

    public void setUsername(JTextField username) {
        this.username = username;
    }

    public JTextField getPassword() {
        return password;
    }

    public void setPassword(JTextField password) {
        this.password = password;
    }

    public JLabel getLuser() {
        return luser;
    }

    public void setLuser(JLabel luser) {
        this.luser = luser;
    }

    public JLabel getLpass() {
        return lpass;
    }

    public void setLpass(JLabel lpass) {
        this.lpass = lpass;
    }

    public JButton getOk() {
        return ok;
    }

    public void setOk(JButton ok) {
        this.ok = ok;
    }

    public JLabel getReg() {
        return reg;
    }

    public void setReg(JLabel reg) {
        this.reg = reg;
    }
}
